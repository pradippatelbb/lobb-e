//
//  LoginVC.swift
//  Traincol
//
//  Created by Krina on 28/02/20.
//  Copyright © 2020 Bhargav Simejiya. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginVC: UIViewController {
 // test
    // Hello Test
    
    @IBOutlet weak var txtEmail: AppTextField! {
        didSet {
            
            txtEmail.placeHolder = "Email Address"
            txtEmail.textField.delegate = self
            txtEmail.textField.keyboardType = .emailAddress
        }
    }
    @IBOutlet weak var txtPassword: AppTextField! {
        didSet {
            txtPassword.setSecureText()
            txtPassword.placeHolder = "Password"
            txtPassword.textField.delegate = self
        }
    }
    
    @IBOutlet weak var btnForotPassword: UIButton! {
        didSet {
            btnForotPassword.titleLabel?.font = appFont(ofSize: 15.0)
        }
    }
    @IBOutlet weak var btnLoin: UIButton! {
        didSet {
            btnLoin.decorateButton(text: "LOGIN")
        }
    }
    @IBOutlet weak var btnRegister: UIButton! {
        didSet {
            btnRegister.titleLabel?.font = appFont(ofSize: 16.0, andType: .bold)
        }
    }
    
    @IBOutlet weak var lblNote: UILabel! {
        didSet {
            lblNote.font = appFont(ofSize: 15.0)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

    // MARK: - Validations
    func isValid() -> Bool {
        guard let email = txtEmail.textField.text else { return false }
        guard let password = txtPassword.textField.text else { return false }
        
        let isEmailAddressValid = email.isValidEmailAddress()
        if email.isEmpty {
            AppNotification.showErrorMessage("Please enter email")
        }
//        else if !isEmailAddressValid {
//            AppNotification.showErrorMessage("Enter valid email address")
//        }
        else if password.isEmpty {
            AppNotification.showErrorMessage("Please enter password")
        } else if password.count < 6 {
            AppNotification.showErrorMessage("Password must be 6 characters long")
        } else {
            return true
        }
        return false
    }

    // MARK: BUTTON ACTIONs
    @IBAction func btnForgotPasswordPressed(_ sender: UIButton) {
        //self.navigationController?.pushViewController(ForgotPasswordVC(), animated: true)
    }
    
    @IBAction func btnLoginPressed(_ sender: UIButton) {
        if isValid() {
            login()
        }
    }

    @IBAction func btnReistrerPressed(_ sender: UIButton) {
        //self.navigationController?.pushViewController(RegistrationVC(), animated: true)
    }

    // MARK: - API CALLING
    func login() {
        var data = [String: Any]()
        data = ["secret_key": "KbzfkZ7pvithUratVRrhv7osHPC8Qiwn",
                "data": ["username": txtEmail.text ?? "",
                         "password": txtPassword.text ?? "",
                         "device_type": "1",
                         "device_token": USER_DEFAULT.string(forKey: DEVICE_TOKEN) ?? ""]]

        SVProgressHUD.show()
        APIManager.shared.login(withParameter: data) { (response) in
            SVProgressHUD.dismiss()
            print(response)
            guard let data = response["data"] as? [String: Any] else {return}
            let status = data["status"] as? Bool ?? false
            if status {
                guard let apiData = data["api_data"] as? [String: Any] else {return}
                USER_DEFAULT.set(apiData, forKey: USER_INFO)
                Constant.isUserLoggedIn = true
                Constant.userToken = apiData["user_token"] as? String ?? ""
                let userRole = apiData["user_role"] as? String ?? ""
                USER_DEFAULT.set(userRole, forKey: USER_TYPE)
                USER_DEFAULT.set("\(apiData["user_id"] as? Int ?? 0)", forKey: USER_ID)
                if userRole == "subscriber" {
                    userType.currenType = .user
//                    self.loginWithFirebaseAuth()
                } else if userRole == "administrator" {
                    userType.currenType = . admin
                }
                APP_DELEGATE.setupHome()
            } else {
                AppNotification.showErrorMessage(data["message"] as? String ?? "")
            }
        }
    }

    
}

//MARK:- TEXFIELD EXTENSION
extension LoginVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtEmail.textField {
            guard let text = txtEmail.textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 48 // Bool
        } else if textField == txtPassword.textField {
            guard let text = txtPassword.textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 16 // Bool
        }
        return true
    }
}
