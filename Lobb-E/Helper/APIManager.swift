//
//  APIManager.swift
//  Attras
//
//  Created by Krina on 11/10/18.
//  Copyright © 2018 Bhargav. All rights reserved.
//

import Alamofire
import Foundation
import CodableAlamofire

//http://52.66.9.229/wp-json/trincol/v1/
private let BASE_URL = "https://trincolltd.com/wp-json/trincol/v1/"
private let secret_key = "secret_key=KbzfkZ7pvithUratVRrhv7osHPC8Qiwn"

private struct APIs {
    //Login
    static let LOGIN = BASE_URL+"login/?"+secret_key

    //Registration
    static let REGISTER = BASE_URL+"register/?"+secret_key
    
    //Forgot Password
    static let FORGOT_PASSWORD = BASE_URL+"forgetPassword/?"+secret_key

    //Get/Edit User Detail
    static let USER_DETAILS = BASE_URL+"getProfile/?"+secret_key
    static let EDIT_USER_DETAILS = BASE_URL+"updateProfile/?"+secret_key

    //Upload Profile photo
    static let UPLOAD_PROFILE = BASE_URL+"uploadImage/?"+secret_key

    //Property List
    static let MAIN_PROPERTY_LIST = BASE_URL+"getAllCategories/?"+secret_key

    //Search Result, Search property result
    static let SEARCH_PROPERTY_LIST = BASE_URL+"getProperty/?"+secret_key
    static let SEARCH_PROPERTY_DETAIL = BASE_URL+"getSingleProperty/?"+secret_key

    //Property Detail from Main Category
    static let PROPERTY_DETAIL_BY_CATEGORY = BASE_URL+"getSinglePropertyByCategory/?"+secret_key

    //Booking History, Booking detail
    static let BOOKING_HISTORY_LIST = BASE_URL+"getAllBookingDetail/?"+secret_key
    static let BOOKING_DETAIL = BASE_URL+"getBookingDetail/?"+secret_key

    //Service List
    static let SERVICE_LIST = BASE_URL+"getServices/?"+secret_key

    //Add Review
    static let ADD_REVIEW = BASE_URL+"addReview/?"+secret_key

    //FAQ List
    static let FAQ_LIST = BASE_URL+"getAllFAQ/?"+secret_key

    //Question List, Create Ticket
    static let QUESTION_LIST = BASE_URL+"getAllQueries/?"+secret_key
    static let CREATE_TICKET = BASE_URL+"CreateTicket/?"+secret_key

    //Approve/Deny Request
    static let ACCEPT_DENY_REQUEST = BASE_URL+"setBookingStatus/?"+secret_key

    //Report
    static let REPORT = BASE_URL+"getReport/?"+secret_key

    //AddBooking
    static let ADD_BOOKING = BASE_URL+"AddBooking/?"+secret_key

    //Notification List
    static let NOTIFICATION_LIST = BASE_URL+"getNotificationList/?"+secret_key

    //Canecl Booking
    static let CANCEL_BOOKING = BASE_URL+"cancelBooking/?"+secret_key
    
    //Logout
    static let LOGOUT = BASE_URL+"logout/?"+secret_key
}

class APIManager {
    static let shared = APIManager()

    var header: [String: String] {
        return ["Authorization": Constant.userToken,
                "Content-Type": "application/json"]
    }

    func login(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.LOGIN)
        AF.request(APIs.LOGIN, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func registration(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.REGISTER)
        AF.request(APIs.REGISTER, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func forgotPassword(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.FORGOT_PASSWORD)
        AF.request(APIs.FORGOT_PASSWORD, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func userDetails(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.USER_DETAILS)
        AF.request(APIs.USER_DETAILS, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func uploadProfile(withProfilePic profilePic : UIImage, complition:@escaping ((_ response: NSDictionary) -> Void)) {
        let imageData = profilePic.jpegData(compressionQuality: 0.1)

        let parameters = ["secret_key": "KbzfkZ7pvithUratVRrhv7osHPC8Qiwn"]
        print("PARAMETER:--", parameters, "urlString:--", APIs.UPLOAD_PROFILE)
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            multipartFormData.append(imageData!, withName: "image1",fileName: "image1.jpeg", mimeType: "image1/jpeg")
        }, to: APIs.UPLOAD_PROFILE).uploadProgress { progress in
            print("Upload Progress: \(progress.fractionCompleted)")
        }.responseJSON { response in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        }
    }

    func logout(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.LOGOUT)
        AF.request(APIs.LOGOUT, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func editUserDetails(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.EDIT_USER_DETAILS)
        AF.request(APIs.EDIT_USER_DETAILS, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func mainPropertyList(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.MAIN_PROPERTY_LIST)
        AF.request(APIs.MAIN_PROPERTY_LIST, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func searchPropertyList(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.SEARCH_PROPERTY_LIST)
        AF.request(APIs.SEARCH_PROPERTY_LIST, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func serviceList(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.SERVICE_LIST)
        AF.request(APIs.SERVICE_LIST, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func searchPropertyDetail(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.SEARCH_PROPERTY_DETAIL)
        AF.request(APIs.SEARCH_PROPERTY_DETAIL, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func bookingList(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.BOOKING_HISTORY_LIST)
        AF.request(APIs.BOOKING_HISTORY_LIST, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func bookingDetail(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.BOOKING_DETAIL)
        AF.request(APIs.BOOKING_DETAIL, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func addReview(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.ADD_REVIEW)
        AF.request(APIs.ADD_REVIEW, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func faqList(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.FAQ_LIST)
        AF.request(APIs.FAQ_LIST, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func questionList(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.QUESTION_LIST)
        AF.request(APIs.QUESTION_LIST, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func createTicket(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.CREATE_TICKET)
        AF.request(APIs.CREATE_TICKET, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func accpetDenyRequest(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.ACCEPT_DENY_REQUEST)
        AF.request(APIs.ACCEPT_DENY_REQUEST, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func report(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.REPORT)
        AF.request(APIs.REPORT, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func addBooking(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.ADD_BOOKING)
        AF.request(APIs.ADD_BOOKING, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func notificationList(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.NOTIFICATION_LIST)
        AF.request(APIs.NOTIFICATION_LIST, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func cancelBooking(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.CANCEL_BOOKING)
        AF.request(APIs.CANCEL_BOOKING, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }

    func propertyDetailByCategory(withParameter param: [String: Any], complition:@escaping ((_ response: NSDictionary) -> Void)) {

        let parameters = param
        print("PARAMETER:--", parameters, "urlString:--", APIs.PROPERTY_DETAIL_BY_CATEGORY)
        AF.request(APIs.PROPERTY_DETAIL_BY_CATEGORY, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let data = response.value as? [String: Any] {
                if let statusCode = response.response?.statusCode {                if statusCode == 401 {
                    Constant.isUserLoggedIn = false
                    Constant.userToken = ""
                    APP_DELEGATE.setupHome()
                } else {
                    complition(data as NSDictionary)
                    }
                }
            } else if let error = response.error {
                print(error.localizedDescription)
            }
        })
    }
}
