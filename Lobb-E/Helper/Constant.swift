//
//  Constant.swift
//  letsChat
//
//  Created by Bhargav Simejiya on 14/10/19.
//  Copyright © 2019 Bhargav Simejiya. All rights reserved.
//

import UIKit

var LANGUAGE = "LANGUAGE"
let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
let DEVICE_TOKEN = "DEVICE_TOKEN"
let USER_DEFAULT = UserDefaults.standard
let USER_TYPE = "USER_TYPE"
let USER_ID = "USER_ID"
let USER_INFO = "USER_INFO"
let ADMIN_FCM_TOKEN = "ADMIN_FCM_TOKEN"
var isChatScreen = false
let ADMIN_NOTIFICATION = "ADMIN_NOTIFICATION"
let USER_NOTIFICATION = "USER_NOTIFICATION"

class Constant {
    // swiftlint:disable all line_length
    static var userToken: String {
        set {
            UserDefaults.standard.set(newValue, forKey: "token")
        }
        get {
            if let token = UserDefaults.standard.string(forKey: "token") {
                return "\(token)"
            }
            return ""
        }
    }
    static var appLanguage: String {
        set {
            Bundle.setLanguage(lang: newValue)
        }
        get {
            return UserDefaults.standard.stringArray(forKey: "AppleLanguage")?.first ?? "en"
        }
    }
    
    static var isUserLoggedIn: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: "isUserLoggedIn")
        }
        get {
            return UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        }
    }
}

// MARK: - Application Type
enum userType: String {
    case user
    case admin
    static var currenType: userType = .user
}

//MARK: - Color constants
struct AppColor {
    static let kAPP_RED: UIColor = UIColor(hexString: "#A70C30") ?? .black
    static let kAPP_FILTER_OPTION: UIColor = UIColor(hexString: "#72A748") ?? .black
}

enum FontType: String {
    case regular = "MyriadPro-Regular"
    case light = "MyriadPro-Light"
//    case medium = "Montserrat-Medium"
    case semiBold = "MyriadPro-Semibold"
    case bold = "MyriadPro-Bold"
}

var isiPhoneSE: Bool {
    return UIScreen.main.bounds.width <= 320.0
}

func appFont(ofSize size: CGFloat, andType type: FontType = .regular) -> UIFont {
    var size = size
    if isiPhoneSE {
        size *= 0.9
    }
    return UIFont.init(name: type.rawValue, size: size) ?? UIFont.systemFont(ofSize: size)
}

func debugLog(_ log: Any...) {
    return print(log)
}

func DPrint(_ items: Any...) {
    if Environment.shouldDebug {
        print(items)
    }
}

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

func setAttributedString(text: String) -> NSAttributedString {
    let attributedString = NSMutableAttributedString()
    attributedString.append(NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: appFont(ofSize: 18.0, andType: .semiBold)]))
    let imgAttachment = NSTextAttachment()
    imgAttachment.image = UIImage(named: "bottomArrow")
    imgAttachment.bounds = CGRect(x: 0, y: 0, width: 12, height: 10)
    let strImage = NSAttributedString(attachment: imgAttachment)
    attributedString.append(strImage)
    
    return attributedString
}

extension UILabel {
    func setHtml(text:String) {

        let strtemp = "<html><body><span style=\"font-family: 'MyriadPro-Regular'; text-align:justify ; font-size: \(self.font!.pointSize)\">\(text)</span></body></html>"
        let data = strtemp.data(using: .utf8)!
        do {
            let attributesString = try NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            return self.attributedText = attributesString
        } catch {
            return self.attributedText = NSAttributedString(string: text)
        }
    }
}
