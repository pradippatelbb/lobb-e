//
//  Data+Extension.swift
//  meritCard
//
//  Created by Krina on 06/02/20.
//  Copyright © 2020 Bhargav Simejiya. All rights reserved.
//

import Foundation

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}
