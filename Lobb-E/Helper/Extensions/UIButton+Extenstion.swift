//
//  UIButton+Extenstion.swift
//  Attras
//
//  Created by Krina on 23/09/19.
//  Copyright © 2019 Krina. All rights reserved.
//

import UIKit

extension UIButton {
    func decorateButton(text: String) {
        self.backgroundColor = AppColor.kAPP_RED
        self.setTitle(text, for: .normal)
        self.layer.cornerRadius = self.layer.frame.size.height / 2
        self.titleLabel?.font = appFont(ofSize: 18.0)
        self.setTitleColor(.white, for: .normal)
    }

    open override func awakeFromNib() {
        super.awakeFromNib()
        if Constant.appLanguage == "ar" {
            if self.contentHorizontalAlignment == .left {
                self.contentHorizontalAlignment = .right
            }
        }
    }
}
