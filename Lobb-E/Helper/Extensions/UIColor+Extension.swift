//
//  UIColorExtension.swift
//  CabMaster
//
//  Created by 200OkMac on 22/08/18.
//  Copyright © 2018 200OK Solutions. All rights reserved.
//

import UIKit

extension UIColor {
    // swiftlint:disable all line_length
    public convenience init?(hexString: String) {
        let red, green, blue, alpha: CGFloat
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = String(hexString[start...])
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                if scanner.scanHexInt64(&hexNumber) {
                    red = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    green = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    blue = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    alpha = CGFloat(hexNumber & 0x000000ff) / 255
                    self.init(red: red, green: green, blue: blue, alpha: alpha)
                    return
                }
            } else if hexColor.count == 6 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                if scanner.scanHexInt64(&hexNumber) {
                    red = CGFloat((hexNumber & 0xff0000) >> 16) / 255
                    green = CGFloat((hexNumber & 0x00ff00) >> 8) / 255
                    blue = CGFloat((hexNumber & 0x0000ff)) / 255
                    alpha = 1.0
                    self.init(red: red, green: green, blue: blue, alpha: alpha)
                    return
                }
            }
        }
        return nil
    }
    convenience init(rgb: Int) {
        self.init(red: CGFloat((rgb >> 16) & 0xFF), green: CGFloat((rgb >> 8) & 0xFF), blue: CGFloat(rgb & 0xFF), alpha: 1)
    }
}
