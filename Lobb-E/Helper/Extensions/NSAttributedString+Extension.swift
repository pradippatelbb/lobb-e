//
//  NSAttributedStringExtension.swift
//  CabMaster
//
//  Created by 200OkMac on 22/08/18.
//  Copyright © 2018 200OK Solutions. All rights reserved.
//

import UIKit

extension NSAttributedString {
    // swiftlint:disable all line_length
    func uppercaseAttributedString() {
        if let mutableAttributedString: NSMutableAttributedString = self.mutableCopy() as? NSMutableAttributedString {
            let copyAttributes = NSMutableArray()
            // Add each set of attributes to the array in a dictionary containing the attributes and range
            let makeRange = NSMakeRange(0, mutableAttributedString.length)
            mutableAttributedString.enumerateAttributes(in: makeRange, options: NSAttributedString.EnumerationOptions(rawValue: 0)) { (attributes, range, _) in
                copyAttributes.add(["attributes": attributes, "range": NSValue(range: range)])
            }
            // Apply attributes to plain uppercaseString
            let string = mutableAttributedString.string.uppercased()
            mutableAttributedString.replaceCharacters(in: makeRange, with: string)
            for attriubute in copyAttributes {
                let dict = attriubute as? [String: Any] ?? [:]
                // swiftlint:disable:next force_cast
                mutableAttributedString.setAttributes(dict["attributes"] as? [NSAttributedString.Key: Any], range: dict["range"] as! NSRange)
            }
        }
    }
}
