//
//  UserDefaultExtension.swift
//  CabMaster
//
//  Created by 200OK-MOB4 on 24/08/18.
//  Copyright © 2018 200OK Solutions. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

extension UserDefaults {
    class func keyExists(_ key: String) -> Bool {
        let object: Any? = self.standard.object(forKey: key)
        if object != nil {
            return true
        }
        return false
    }
    class func coordinateForKey(_ key: String) -> CLLocationCoordinate2D {
        var theCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2D()
        let theOblect = self.standard.object(forKey: key) as AnyObject
        if let str = theOblect as? String {
            let theComponents = str.split(separator: ",")
            if theComponents.count == 2 {
                theCoordinate.latitude = Double(theComponents[0])!
                theCoordinate.longitude = Double(theComponents[1])!
            }
        }
        return theCoordinate
    }
    class func setCoordinate(_ coordinate: CLLocationCoordinate2D, forKey key: String) {
        let coordString = "\(coordinate.latitude)" + "\(coordinate.longitude)"
        self.standard.set(coordString, forKey: key)
        self.standard.synchronize()
    }
    /* convenience method to save a given bool for a given key */
    class func saveBool(_ object: Bool, forKey key: String) {
        let defaultes = self.standard
        defaultes.set(object, forKey: key)
        defaultes.synchronize()
    }
    /* convenience method to save a given Int for a given key */
    class func saveInt(_ object: Int, forKey key: String) {
        let defaultes = self.standard
        defaultes.set(object, forKey: key)
        defaultes.synchronize()
    }
    /* convenience method to save a given string for a given key */
    class func saveObject(_ object: Any, forKey key: String) {
        let defaults = self.standard
        defaults.set(object, forKey: key)
        defaults.synchronize()
    }

    /* convenience method to return a string for a given key */
    class func retrieveObjectForKey(_ key: String) -> Any {
        return self.standard.object(forKey: key) as Any
    }
    /* convenience method to delete a value for a given key */
    class func deleteObjectForKey(_ key: String) {
        let defaults = self.standard
        defaults.removeObject(forKey: key)
        defaults.synchronize()
    }

    /* convenience method to clear userdefaults for current application */
    class func clearUserDefaults() {
        let appDomain = Bundle.main.bundleIdentifier
        self.standard.removePersistentDomain(forName: appDomain!)
    }
}
