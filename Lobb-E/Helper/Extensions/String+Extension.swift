//
//  StringExtension.swift
//  CabMaster
//
//  Created by 200OkMac on 22/08/18.
//  Copyright © 2018 200OK Solutions. All rights reserved.
//

import UIKit
import Foundation

extension String {
    // swiftlint:disable all line_length
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }

    var spacesTrimmed: String {
        var newString = self
        while newString.last?.isWhitespace == true {
            newString = String(newString.dropLast())
        }
        
        while newString.first?.isWhitespace == true {
            newString = String(newString.dropFirst())
        }
        return newString
    }

    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

extension String {
    public func toInt() -> Int? {
        if let num = NumberFormatter().number(from: self) {
            return num.intValue
        } else {
            return nil
        }
    }

    func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }

    func convertDate(timeStamp : Double ) -> String {
        let startDate = Date.init(timeIntervalSince1970: timeStamp)
    
        let outputTimeZone = NSTimeZone.local
        let outputDateFormatter = DateFormatter()
        outputDateFormatter.locale = Locale.init(identifier: "en_US_POSIX")
        outputDateFormatter.timeZone = outputTimeZone
        outputDateFormatter.dateFormat = "HH:mm a"
        
        let strDate1 = outputDateFormatter.string(from: startDate as Date)
        return strDate1
    }

    func getDictionaryFromJSON() -> [String: Any] {
        var dictonary:[String: Any]?
        if let data = self.data(using: String.Encoding.utf8) {
            do {
                dictonary = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: Any]
                
                if let myDictionary = dictonary {
                    return myDictionary
                }
            } catch let error as NSError {
                print(error)
            }
        }
        return [:]
    }
    func getArrayFromJSON() -> [Any] {
        var array: [Any]?
        if let data = self.data(using: String.Encoding.utf8) {
            do {
                array = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [Any]
                if let myArray = array {
                    return myArray
                }
            } catch let error as NSError {
                print(error)
            }
        }
        return []
    }
    func getAttributedString() -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        return attributedString
    }
    func isValidEmailAddress() -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0 {
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
}

extension NSMutableAttributedString {
    func addColor(_ color: UIColor, forText text: String? = nil) -> NSMutableAttributedString {
        let text = text ?? self.string
        let range = (self.string as NSString).range(of: text)
        self.addAttribute(.foregroundColor, value: color, range: range)
        return self
    }
    func addFont(_ font: UIFont, forText text: String? = nil) -> NSMutableAttributedString {
        let text = text ?? self.string
        let range = (self.string as NSString).range(of: text)
        self.addAttribute(.font, value: font, range: range)
        return self
    }
}

extension StringProtocol where Index == String.Index {
    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }
}

extension String {
    func grouping(every groupSize: String.IndexDistance, with separator: Character) -> String {
       let cleanedUpCopy = replacingOccurrences(of: String(separator), with: "")
       return String(cleanedUpCopy.enumerated().map() {
            $0.offset % groupSize == 0 ? [separator, $0.element] : [$0.element]
       }.joined().dropFirst())
    }
}

extension Double {
    func rounded(digits: Int) -> Double {
        let multiplier = pow(10.0, Double(digits))
        return (self * multiplier).rounded() / multiplier
    }
}
