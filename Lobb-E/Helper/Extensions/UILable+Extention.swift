//
//  UILableExtention.swift
//  vSnap
//
//  Created by Krina on 25/09/18.
//  Copyright © 2018 Nirav Joshi. All rights reserved.
//

import UIKit

extension UILabel {
    open override func awakeFromNib() {
        super.awakeFromNib()
        if Constant.appLanguage == "ar" {
            if textAlignment == .left {
                self.textAlignment = .right
            }
        }
    }

    func setRegularFont(withSize size: CGFloat) {
        self.font = UIFont(name: "", size: size)
    }

    func setBoldFont(withSize size: CGFloat) {
        self.font = UIFont(name: "", size: size)
    }

    func setTextColor(withString hexString: String) {
        self.textColor = UIColor(hexString: hexString)
    }
}
