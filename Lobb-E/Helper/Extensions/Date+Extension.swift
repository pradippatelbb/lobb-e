//
//  DateExtension.swift
//  CabMaster
//
//  Created by 200OK-IOS4 on 20/09/18.
//  Copyright © 2018 200OK Solutions. All rights reserved.
//

import UIKit

// swiftlint:disable all line_length
let D_SECONDS       = 60
let D_MINUTE        = 60
let D_HOUR          = 3600
let D_DAY           = 86400
let D_WEEK          = 604800
let D_YEAR          = 31556926
let componentFlags = Set<Calendar.Component>([.year, .month, .day, .weekOfMonth, .weekOfYear, .hour, .minute, .second, .weekday, .weekdayOrdinal])

extension Date {
    func getUTCFormateDate() -> String? {
        let dateFormatter = DateFormatter()
        let timeZone = NSTimeZone(name: "UTC")
        dateFormatter.timeZone = timeZone as TimeZone?
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        var dateString: String? = nil
        dateString = dateFormatter.string(from: self)
        dateString = dateString?.replacingOccurrences(of: " ", with: "T")
        dateString = (dateString ?? "") + ("+00:00")
        return dateString
    }

    static func currentCalendar() -> Calendar {
        var sharedCalender: Calendar? = nil
        if sharedCalender == nil {
            sharedCalender = Calendar.autoupdatingCurrent
        }
        return sharedCalender!
    }

    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }

    // MARK: - Relative Dates
    mutating func dateWithDaysFromNow(days: Int) -> Date {
        return Date().dateByAddingDays(dDays: days)
    }
    mutating func dateWithDaysBeforeNow(days: Int) -> Date {
        return Date().dateBySubtractingDays(dDays: days)
    }
    mutating func dateTomorrow() -> Date {
        return Date().dateByAddingDays(dDays: 1)
    }

    mutating func dateYesterday() -> Date {
        return Date().dateBySubtractingDays(dDays: 1)
    }

    mutating func dateWithHoursFromNow(dHours: Int) -> Date {
        let aTimeInterval = Int(Date().timeIntervalSinceReferenceDate) + D_HOUR * dHours
        let newDate = Date.init(timeIntervalSinceReferenceDate: TimeInterval(aTimeInterval))
        return newDate
    }
//    + (NSDate *) dateWithHoursBeforeNow: (NSInteger) dHours
//    {
//    NSTimeInterval aTimeInterval = [[NSDate date] timeIntervalSinceReferenceDate] - D_HOUR * dHours;
//    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
//    return newDate;
//    }
//
//    + (NSDate *) dateWithMinutesFromNow: (NSInteger) dMinutes
//    {
//    NSTimeInterval aTimeInterval = [[NSDate date] timeIntervalSinceReferenceDate] + D_MINUTE * dMinutes;
//    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
//    return newDate;
//    }
//
//    + (NSDate *) dateWithMinutesBeforeNow: (NSInteger) dMinutes
//    {
//    NSTimeInterval aTimeInterval = [[NSDate date] timeIntervalSinceReferenceDate] - D_MINUTE * dMinutes;
//    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
//    return newDate;
//    }
    // Courtesy of dedan who mentions issues with Daylight Savings
    
    func dateByAddingDays(dDays:Int) -> Date {
        var dateComponents = DateComponents()
        dateComponents.day = dDays
        let newDate = Calendar.current.date(byAdding: dateComponents, to: self)
        return newDate!
    }
    
    func dateBySubtractingDays(dDays:Int) -> Date {
        return self.dateByAddingDays(dDays: dDays * -1)
    }

    func dateByAddingHours(dHours:Int) -> Date {
        let aTimeInterval = Int(self.timeIntervalSinceReferenceDate) + D_HOUR * dHours
        let newDate = Date.init(timeIntervalSinceReferenceDate: TimeInterval(aTimeInterval))
        return newDate
    }
    
    func dateBySubtractingHours(dHours:Int) -> Date {
        return self.dateByAddingHours(dHours: dHours * -1)
    }
    
    func dateByAddingMinutes(dMinutes:Int) -> Date {
        let aTimeInterval = Int(self.timeIntervalSinceReferenceDate) + D_MINUTE * dMinutes
        let newDate = Date.init(timeIntervalSinceReferenceDate: TimeInterval(aTimeInterval))
        return newDate
    }

    func dateBySubtractingMinutes(dHours:Int) -> Date {
        return self.dateByAddingHours(dHours: dHours * -1)
    }
    
    func dateByAddingSeconds(dSeconds:Int) -> Date {
        let aTimeInterval = Int(self.timeIntervalSinceReferenceDate) + D_SECONDS * dSeconds
        let newDate = Date.init(timeIntervalSinceReferenceDate: TimeInterval(aTimeInterval))
        return newDate
    }
    
    func componentsWithOffsetFromDate(aDate:Date) -> DateComponents {
        let dTime = Date.currentCalendar().dateComponents(componentFlags, from: self)
        return dTime
    }
    
    // MARK : Extremes
    
    func dateAtStartOfDay() -> Date {
        var components = Date.currentCalendar().dateComponents(componentFlags, from: self)
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Date.currentCalendar().date(from: components)!
    }
    
    func dateAtEndOfDay() -> Date {
        var components = Date.currentCalendar().dateComponents(componentFlags, from: self)
        components.hour = 23
        components.minute = 59
        components.second = 59
        return Date.currentCalendar().date(from: components)!
    }

    // MARK : Retrieving Intervals
    
    func minutesAfterDate(aDate:Date) -> Int {
        let ti = self.timeIntervalSince(aDate)
        return Int(ti)/D_MINUTE
    }
    
    func minutesBeforeDate(aDate:Date) -> Int {
        let ti = aDate.timeIntervalSince(self)
        return Int(ti)/D_MINUTE
    }

    func hoursAfterDate(aDate:Date) -> Int {
        let ti = self.timeIntervalSince(aDate)
        return Int(ti)/D_HOUR
    }
    
    func hoursBeforeDate(aDate:Date) -> Int {
        let ti = aDate.timeIntervalSince(self)
        return Int(ti)/D_HOUR
    }
    
    func daysAfterDate(aDate:Date) -> Int {
        let ti = self.timeIntervalSince(aDate)
        return Int(ti)/D_DAY
    }
    
    func daysBeforeDate(aDate:Date) -> Int {
        let ti = aDate.timeIntervalSince(self)
        return Int(ti)/D_DAY
    }
    
    func distanceInDaysToDate(anotherDate:Date) -> Int {
        let gregorianCalender = Calendar.init(identifier: Calendar.Identifier.gregorian)
        let components = gregorianCalender.dateComponents(componentFlags, from: self, to: anotherDate)
        return components.day!
    }
    
    // MARK : Decomposing Dates
    
    func nearestHour() -> Int {
        let aTimeInterval = Int(Date().timeIntervalSinceReferenceDate) + D_MINUTE * 30
        let newDate = Date.init(timeIntervalSinceReferenceDate: TimeInterval(aTimeInterval))
        let components = Date.currentCalendar().dateComponents(componentFlags, from: newDate)
        return components.hour!
    }

    func hour() -> Int {
        let components = Date.currentCalendar().dateComponents(componentFlags, from: self)
        return components.hour!
    }
    
    func minute() -> Int {
        let components = Date.currentCalendar().dateComponents(componentFlags, from: self)
        return components.minute!
    }
    
    func second() -> Int {
        let components = Date.currentCalendar().dateComponents(componentFlags, from: self)
        return components.second!
    }
    
    func day() -> Int {
        let components = Date.currentCalendar().dateComponents(componentFlags, from: self)
        return components.day!
    }

    func month() -> Int {
        let components = Date.currentCalendar().dateComponents(componentFlags, from: self)
        return components.month!
    }
    
    func week() -> Int {
        let components = Date.currentCalendar().dateComponents(componentFlags, from: self)
        return components.weekday!
    }
    
    func weekday() -> Int {
        let components = Date.currentCalendar().dateComponents(componentFlags, from: self)
        return components.weekday!
    }

    func nthWeekday() -> Int {
        let components = Date.currentCalendar().dateComponents(componentFlags, from: self)
        return components.weekdayOrdinal!
    }

    func year() -> Int {
        let components = Date.currentCalendar().dateComponents(componentFlags, from: self)
        return components.year!
    }
    
    func toString(format: String = "dd/MM/yyyy") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func getDateStringInISO860() -> String {
        let dateFormattor = DateFormatter()
        let enUSPOSIXLocale = Locale.init(identifier: "en_US_POSIX")
        dateFormattor.locale = enUSPOSIXLocale
        dateFormattor.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let iso8601String = dateFormattor.string(from: self)
        return iso8601String
        
    }
}
