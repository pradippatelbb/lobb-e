//
//  UIViewController+Extension.swift
//  Attras
//
//  Created by Krina on 20/09/19.
//  Copyright © 2019 Krina. All rights reserved.
//

import UIKit
import SVProgressHUD

extension UIViewController {
    // swiftlint:disable all line_length
    func showAlert(title: String, message: String? = nil, actionName: String = "Okay", action buttonAction:(() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: actionName, style: .default, handler: { (_) in
            buttonAction?()
        }))
        present(alert, animated: true, completion: nil)
    }
    func showAlert(title: String, message: String? = nil, actions: UIAlertAction...) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        actions.forEach({alert.addAction($0)})
        present(alert, animated: true, completion: nil)
    }
    
    func addBackgroundImage() {
        self.view.addBackgroundImage()
    }
    
    func addShadowToBar() {
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = AppColor.kAPP_RED.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 3.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 5
    }
    
    func addBackButton() {
        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "backButton"), style: .plain, target: self, action: #selector(popOrDismiss(_:)))
        barButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = barButton
    }

    func logOut() {
        let alert = UIAlertController(title: "ALERT", message: "Are you sure you want to logout?",
                                      preferredStyle: UIAlertController.Style.alert)
        let actionOk = UIAlertAction(title: "Yes", style: .default) { (_) in
            self.logoutAPI()
        }

        let actionNo = UIAlertAction(title: "No", style: .cancel) { (_) in
        }
        alert.addAction(actionOk)
        alert.addAction(actionNo)
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - API CALLING
    func logoutAPI() {
        var userId = Int()
        if let data = USER_DEFAULT.object(forKey: USER_INFO) as? [String: Any] {
            userId = data["user_id"] as? Int ?? 0
        }
        var data = [String: Any]()
        data = ["secret_key": "KbzfkZ7pvithUratVRrhv7osHPC8Qiwn",
                "data": ["user_id": userId,
                         "user_token": Constant.userToken]]

        SVProgressHUD.show()
        APIManager.shared.logout(withParameter: data) { (response) in
            SVProgressHUD.dismiss()
            print(response)
            guard let data = response["data"] as? [String: Any] else {return}
            let status = data["status"] as? Bool ?? false
            if status {
                Constant.isUserLoggedIn = false
                APP_DELEGATE.setupHome()
            } else {
                AppNotification.showErrorMessage(data["message"] as? String ?? "")
            }
        }
    }

    // MARK: - IBButton Action
    @objc @IBAction func popOrDismiss(_ sender: UIButton? = nil) {
        view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
}

extension UIScrollView {
    func scrollTo(horizontalPage: Int? = 0, verticalPage: Int? = 0, animated: Bool? = true) {
        var frame: CGRect = self.frame
        frame.origin.x = frame.size.width * CGFloat(horizontalPage ?? 0)
        frame.origin.y = frame.size.width * CGFloat(verticalPage ?? 0)
        self.scrollRectToVisible(frame, animated: animated ?? true)
    }
}
