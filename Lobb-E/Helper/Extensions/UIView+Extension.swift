//
//  UIViewExtension.swift
//  CabMaster
//
//  Created by 200OkMac on 22/08/18.
//  Copyright © 2018 200OK Solutions. All rights reserved.
//

import UIKit

extension UIView {
    // swiftlint:disable all line_length
    
    final func animateScaleUpDown(onCompletion: @escaping (() -> Void)) {
        animateScaleDown(duration: 0.05, scale: 0.9)
        animateScaleIdentity(duration: 0.05, delay: 0.05)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { onCompletion() }
    }
    
    final func animateScaleDown(duration: TimeInterval = 0.1,
                                scale: CGFloat = 0.95) {
        UIView.animate(withDuration: duration,
                       delay: 0.0,
                       options: .curveEaseOut,
                       animations: {
                        self.transform = CGAffineTransform(scaleX: scale, y: scale)
        },
                       completion: nil)
    }
    
    final func animateScaleIdentity(duration: TimeInterval = 0.1,
                                    delay: TimeInterval = 0) {
        UIView.animate(withDuration: duration,
                       delay: delay,
                       options: .curveEaseIn,
                       animations: {
                        self.transform = .identity
        },
                       completion: nil)
    }
    
    func circle() {
        circleOfDiameter(diameter: self.bounds.size.width)
    }
    func circleOfDiameter(diameter: CGFloat) {
        let viewCenter = self.center
        let newFrame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: diameter, height: diameter)
        self.frame = newFrame
        self.layer.cornerRadius = diameter/2.0
        self.center = viewCenter
        self.clipsToBounds = true
        self.layer.masksToBounds = true
    }
    func shadow(withRoundedCorners containerView: UIView?) {
        layer.cornerRadius = 10.0
        layer.masksToBounds = true
        shadow(containerView)
    }
    func shadow(_ containerView: UIView?) {
        backgroundColor = UIColor.white
        containerView?.backgroundColor = UIColor.clear
        containerView?.layer.shadowOffset = CGSize(width: 0, height: 0)
        containerView?.layer.shadowColor = UIColor.black.cgColor
        containerView?.layer.shadowOpacity = 0.33
        containerView?.layer.shadowRadius = 1.0
    }
    func addDropShadow(_ color: UIColor, withOffset offset: CGSize, radius: CGFloat, opacity: CGFloat) {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shadowOpacity = Float(opacity)
    }
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    func presentViewFromRight() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        self.layer.add(transition, forKey: "pushLeft")
    }
    func presentViewFromBottom() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        self.layer.add(transition, forKey: "pushBottom")
    }
    func presentViewFromLeft(block: @escaping (_ success: Bool) -> Void) {
        CATransaction.begin()
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        CATransaction.setCompletionBlock {
            block(true)
        }
        self.layer.add(transition, forKey: "pushLeft")
        CATransaction.commit()
    }
    func presentViewFromTop(block: @escaping (_ success: Bool) -> Void) {
        CATransaction.begin()
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        CATransaction.setCompletionBlock {
            block(true)
        }
        self.layer.add(transition, forKey: "pushTop")
        CATransaction.commit()
    }
    func shake(count: Float = 6, for duration: TimeInterval = 0.5, withTranslation translation: Float = 5) {
        let animation: CABasicAnimation = CABasicAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.repeatCount = count
        animation.duration = duration/TimeInterval(animation.repeatCount)
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: CGFloat(-translation), y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: CGFloat(translation), y: self.center.y))
        layer.add(animation, forKey: "shake")
    }
    func addBorder(ofSize size: CGFloat, color: UIColor, cornerRadius: CGFloat = 0.0) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = size
        self.layer.borderColor = color.cgColor
        self.clipsToBounds = true
    }
    
    func addBackgroundImage() {
        var background = UIImage()
        if UIDevice.current.userInterfaceIdiom == .pad {
            background = UIImage(named: "bg-img")!
        } else {
            background = UIImage(named: "bg-img")!
        }
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: bounds)
        imageView.contentMode =  .center
        imageView.image = background
        addSubview(imageView)
        self.sendSubviewToBack(imageView)
    }
    
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }
    
    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
}

extension Bundle {
    private static var bundle: Bundle!

    public static func localizedBundle() -> Bundle! {
        if bundle == nil {
            let appLang = UserDefaults.standard.string(forKey: "app_lang") ?? "ru"
            let path = Bundle.main.path(forResource: appLang, ofType: "lproj")
            bundle = Bundle(path: path!)
        }

        return bundle;
    }

    public static func setLanguage(lang: String) {
        UserDefaults.standard.set([lang], forKey: "AppleLanguage")
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        bundle = Bundle(path: path!)
    }

    static func swizzleLocalization() {
        let orginalSelector = #selector(localizedString(forKey:value:table:))
        guard let orginalMethod = class_getInstanceMethod(self, orginalSelector) else { return }

        let mySelector = #selector(myLocaLizedString(forKey:value:table:))
        guard let myMethod = class_getInstanceMethod(self, mySelector) else { return }

        if class_addMethod(self, orginalSelector, method_getImplementation(myMethod), method_getTypeEncoding(myMethod)) {
            class_replaceMethod(self, mySelector, method_getImplementation(orginalMethod), method_getTypeEncoding(orginalMethod))
        } else {
            method_exchangeImplementations(orginalMethod, myMethod)
        }
    }

    @objc private func myLocaLizedString(forKey key: String,value: String?, table: String?) -> String {
        guard let bundlePath = Bundle.main.path(forResource: APP_DELEGATE.currentLanguage, ofType: "lproj"),
            let bundle = Bundle(path: bundlePath) else {
                return Bundle.main.myLocaLizedString(forKey: key, value: value, table: table)
        }
        return bundle.myLocaLizedString(forKey: key, value: value, table: table)
    }
}

extension String {
    func localized() -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.localizedBundle(), value: "", comment: "")
    }

    func localizeWithFormat(arguments: CVarArg...) -> String{
        return String(format: self.localized(), arguments: arguments)
    }

    func localizedString(forKey key: String) -> String {
        var result = Bundle.main.localizedString(forKey: key, value: nil, table: nil)

        if result == key {
            result = Bundle.main.localizedString(forKey: key, value: nil, table: nil)
        }

        return result
    }
}
extension UIView {
  func addDashedBorder() {
    let color = UIColor.white.cgColor

    let shapeLayer:CAShapeLayer = CAShapeLayer()
    let frameSize = self.frame.size
    let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

    shapeLayer.bounds = shapeRect
    shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
    shapeLayer.fillColor = UIColor.clear.cgColor
    shapeLayer.strokeColor = color
    shapeLayer.lineWidth = 2
    shapeLayer.lineJoin = CAShapeLayerLineJoin.round
    shapeLayer.lineDashPattern = [6,3]
    shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 15).cgPath

    self.layer.addSublayer(shapeLayer)
    }

    func elevate(elevation: Double, radius: Double? = nil, color:UIColor = .black) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: elevation)
        self.layer.shadowRadius = CGFloat(elevation)
        self.layer.shadowOpacity = 0.5
        self.layer.cornerRadius = CGFloat(radius ?? elevation)
        self.clipsToBounds = false
    }
}
