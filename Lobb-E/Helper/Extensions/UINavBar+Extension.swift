//
//  UINavBar+Extension.swift
//  Attras
//
//  Created by Krina on 30/09/19.
//  Copyright © 2019 Krina. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationItem {

    func setTitle(title: String, subtitle: NSAttributedString) {

        let one = UILabel()
        one.text = title
        one.font = appFont(ofSize: 17.0, andType: .regular)
        one.textAlignment = .center
        one.textColor = .white
        one.sizeToFit()

        let two = UILabel()
        two.attributedText = subtitle
        two.font = appFont(ofSize: 12.0, andType: .regular)
        two.textAlignment = .center
        two.textColor = .white
        two.sizeToFit()

        let stackView = UIStackView(arrangedSubviews: [one, two])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical

        let width = max(one.frame.size.width, two.frame.size.width)
        stackView.frame = CGRect(x: 0, y: 0, width: width, height: 35)

        one.sizeToFit()
        two.sizeToFit()

        self.titleView = stackView
    }
}
