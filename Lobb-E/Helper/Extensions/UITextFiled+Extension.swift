//
//  UITextFiled+Extension.swift
//  Attras
//
//  Created by Krina on 11/10/19.
//  Copyright © 2019 Krina. All rights reserved.
//

import UIKit

extension UITextView {
    open override func awakeFromNib() {
        super.awakeFromNib()
        if Constant.appLanguage == "ar" {
            if self.textAlignment == .left || self.textAlignment == .natural {
                self.textAlignment = .right
            }
        }
    }
}
