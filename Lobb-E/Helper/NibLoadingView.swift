import UIKit

// Usage: Subclass your UIView from NibLoadingView to automatically load a xib with the same name as your class

protocol NibDefinable {
    var nibName: String { get }
}

class NibLoadingView: UIView, NibDefinable {

    @IBOutlet weak var view: UIView!

    var nibName: String {
        return String(describing: type(of: self))
    }

    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }

    private func nibSetup() {
        backgroundColor = .clear

        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true

        addSubview(view)

        viewDidLoad()
    }

    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        guard let nibView = nib.instantiate(withOwner: self, options: nil)
            .first as? UIView
            else { fatalError("NibView: Error") }

        return nibView
    }

    func viewDidLoad() {}

}
