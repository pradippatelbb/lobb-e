//
//  Environment.swift
//  Immerch
//
//  Created by Ravi Goswami on 05/09/19.
//  Copyright © 2019 Immerch. All rights reserved.
//

import Foundation

enum Environment: String {
    
    case development, production, production_debug
    
    static var current: Environment {
        guard let environmentName = Bundle.main.object(forInfoDictionaryKey: "AppEnvironment") as? String
            else { fatalError("Unknown application environment") }
        
        guard let environment = Environment(rawValue: environmentName)
            else { fatalError("""
                Environment inference failed!
                Environment Name:\(environmentName)
                There is no environment type configured for above name inside app
                """) }
        
        return environment
    }
    
    static var shouldDebug: Bool = {
        return [Environment.development, Environment.production_debug].contains(current)
    }()
}
