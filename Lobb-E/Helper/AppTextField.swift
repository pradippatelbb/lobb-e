//
//  AppTextField.swift
//  letsChat
//
//  Created by Bhargav Simejiya on 14/10/19.
//  Copyright © 2019 Bhargav Simejiya. All rights reserved.
//

import UIKit

class AppTextField: NibLoadingView {

    @IBOutlet weak var lblLine: UILabel!
    @IBOutlet weak var btnTap: UIButton!
    @IBOutlet weak var textField: UITextField!{
        didSet{
            textField.delegate = self
            textField.font = appFont(ofSize: 17.0)
            textField.clipsToBounds = true
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.textField.frame.height))
            textField.leftView = paddingView
            textField.leftViewMode = UITextField.ViewMode.always
            textField.textColor = .gray
        }
    }
    
    @IBOutlet weak var imgLeading: UIImageView! {
        didSet {
            imgLeading.isHidden = true
        }
    }
    
    @IBOutlet weak var imgTrailing: UIImageView!{
        didSet {
            imgTrailing.isHidden = true
        }
    }
    
    func setSecureText() {
        textField.isSecureTextEntry = true
    }
    
    func setLeadingImage(_ img:UIImage?) {
        imgLeading.image = img
        imgLeading.isHidden = img == nil
    }
    
    func setTrailingImage(_ img:UIImage?) {
        imgTrailing.image = img
        imgTrailing.isHidden = img == nil
    }
    
    var text: String? {
        return textField.text
    }
    
    var font: UIFont? = nil {
        didSet {
            textField.font = font
        }
    }
    
    var placeHolder: String = "" {
        didSet {
            textField.placeholder = placeHolder
            textField.attributedPlaceholder = NSAttributedString(string: placeHolder, attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        }
    }
}

extension AppTextField: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
