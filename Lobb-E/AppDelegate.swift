//
//  AppDelegate.swift
//  Lobb-E
//
//  Created by Bhargav on 17/09/20.
//  Copyright © 2020 Bhargav. All rights reserved.
//

import UIKit
import IQKeyboardManager

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    var currentLanguage: String = "en"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setupKeyBoard()
        setupHome()
        
        currentLanguage = Constant.appLanguage
        Bundle.setLanguage(lang: currentLanguage)
        Bundle.swizzleLocalization()
        if currentLanguage == "en" {
            Bundle.swizzleLocalization()
        }
        
        return true
    }
    
    
    // MARK: IQKeyBoard setup
       func setupKeyBoard() {
           IQKeyboardManager.shared().isEnabled = true
           IQKeyboardManager.shared().isEnableAutoToolbar = true
           IQKeyboardManager.shared().shouldResignOnTouchOutside = true
       }

       func setupHome() {
        
           let navC = UINavigationController(rootViewController: LoginVC())
           navC.navigationBar.isHidden = true
           self.window?.rootViewController = navC
           self.window?.makeKeyAndVisible()
        
       }
    
}

